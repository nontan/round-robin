import os
import discord
import asyncio
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

TOKEN = os.getenv('DISCORD_BOT_ACCESS_TOKEN')
MAIN_CHANNEL_ID = int(os.getenv('DISCORD_MAIN_CHANNEL_ID'))
READY_MESSAGE = 'コミュニティマネジメントボット Round Robin 起動...'
ACCEPT_STAMP = '👍'
BOUNCE_TIME = 60 * 15
# BOUNCE_TIME = 5
JOIN_RESPONSE_FOR_STRANGER = '{mention} 様、湘南藤沢Projectsへようこそ。こちらは湘南藤沢から「仕掛け、続ける」人々のコミュニティになります。' \
    '参加を継続するには15分以内に自己紹介を書いて頂く必要があります。ピン留めされたこれまでのメンバーの自己紹介を参考に、' \
    '本チャンネルに書き込みをお願いいたしますm(_ _)m'
JOIN_RESPONSE_FOR_MEMBERS = f'コミュニティメンバーの皆様は自己紹介を確認し次第、速やかにその投稿に承認のリアクション({ACCEPT_STAMP})をつけてください。'
ACCEPT_MESSAGE = '{authorizer}様が{observed_member}様の自己紹介を承認しました。'


PRODUCTION = True

observed_member_list = set()
observed_message_list = set()

client = discord.Client()

@client.event
async def on_ready():
    logger.debug(READY_MESSAGE)
    channel = client.get_channel(MAIN_CHANNEL_ID)
    logger.debug(f'channel: {channel}')
    await channel.send(READY_MESSAGE)

@client.event
async def on_member_join(member):
    channel = client.get_channel(MAIN_CHANNEL_ID)
    await channel.send(JOIN_RESPONSE_FOR_STRANGER.format(mention=member.mention))
    await channel.send(JOIN_RESPONSE_FOR_MEMBERS)
    observed_member_list.add(member)
    logger.debug(f'observed_member_list: {observed_member_list}')
    
    # 15分間待機
    await asyncio.sleep(BOUNCE_TIME)

    # userがobserved_member_listにいないか確認
    if member not in observed_member_list:
        return

    await member.kick(reason="15分以内に自己紹介が承認されなかったため")
    channel = client.get_channel(MAIN_CHANNEL_ID)
    await channel.send(f'自己紹介の承認が確認されなかったため、{member.mention}様を執行(KICK)しました。')
                    
@client.event
async def on_message(message):
    if len(observed_member_list) == 0:
        return
    if message.author in observed_member_list:
        observed_message_list.add(message)
        logger.debug(f'observed_message_list: {observed_message_list}')

@client.event
async def on_reaction_add(reaction, user):
    if not reaction.message in observed_message_list:
        return
    # リアクションが:thumbsup:でかつ、userが観察対象のメッセージの発言者でないとき、
    # TODO: thumbsupでいいのか?
    logger.debug(f'reaction.emoji: {reaction.emoji}')
    if str(reaction.emoji) == ACCEPT_STAMP and user not in observed_member_list:
        # observed_member_listからメッセージの発言者を消す
        observed_member_list.remove(reaction.message.author)
        logger.debug(f'observed_member_list: {observed_member_list}')
        # メッセージをピン留め

        # userが承認したことを通知
        response = ACCEPT_MESSAGE.format(
            authorizer=user.mention,
            observed_member=reaction.message.author.mention                                    
        ) 
        logger.debug(response)
        await reaction.message.channel.send(response)

async def kick_user(user):
    await client.kick(user, KICK_REASON="15分以内に自己紹介が承認されなかったため")
    channel = client.get_channel(MAIN_CHANNEL_ID)
    await channel.send(f'自己紹介の承認が確認されなかったため、{user.mention}を執行(KICK)しました。')

def main():
    client.run(TOKEN)


if __name__ == '__main__':
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    main()
